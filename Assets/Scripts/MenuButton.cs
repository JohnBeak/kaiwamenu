﻿using UnityEngine;
using UnityEngine.UI;

public class MenuButton : MonoBehaviour
{
    #region Data
    private Button AssociatedButton;
    private Image Icon;
    private GameObject Lock;
    private Text Label;

    private Color IconActiveColor = Color.white;
    private Color IconInactiveColor = new Color(1, 1, 1, 0.25f);

    private bool OldIsActive;
    #endregion

    #region Interface
    public bool IsActive;

    public void Awake()
    {
        AssociatedButton = GetComponent<Button>();
        Icon = transform.Find("Icon").GetComponent<Image>();
        Lock = transform.Find("Lock").gameObject;
        Label = transform.Find("Label").GetComponent<Text>();
    }

    public void Start()
    {
        if (IsActive)
        {
            Activate();
        }
        else
        {
            Disable();
        }

        OldIsActive = IsActive;
    }

    public void Update()
    {
        if (IsActive != OldIsActive)
        {
            if (IsActive)
            {
                Activate();
            }
            else
            {
                Disable();
            }

            OldIsActive = IsActive;
        }
    }
    #endregion

    #region Implementation
    private void Activate()
    {
        AssociatedButton.interactable = true;
        Icon.color = IconActiveColor;
        Label.color = IconActiveColor;
        Lock.SetActive(false);
    }

    private void Disable()
    {
        AssociatedButton.interactable = false;
        Icon.color = IconInactiveColor;
        Label.color = IconInactiveColor;
        Lock.SetActive(true);
    }
    #endregion
}